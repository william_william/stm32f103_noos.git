

Stack_Size      EQU     0x00000400  				;定义堆栈大小为1024byte
				AREA    STACK, NOINIT, READWRITE, ALIGN=3  ;定义一个数据段,标记为STACK，即栈，不写入初始值初，对RAM来说，即初始化为0，8字节对齐
Stack_Mem		SPACE	Stack_Size    				;保留Stack_Size大小的栈空间
__initial_sp  										;标号，代表堆栈顶部地址，后面有用

                PRESERVE8							;指示编译器8字节对齐
                THUMB								;指示编译器以后的指令为THUMB指令								


; Vector Table Mapped to Address 0 at Reset
				AREA    RESET, CODE, READONLY		;定义只读数据段，标记为RESET，其实放在CODE区，位于0地址
				EXPORT  __Vectors					;在程序中声明一个全局的标号__Vectors，该标号可在其他的文件中引用
					
__Vectors       DCD     __initial_sp				;当前地址写入一个字(32bit)数据，值应该为栈顶地址
                DCD     Reset_Handler              	;当前地址写入一个字(32bit)数据，值为Reset_Handler指向的地址值，即程序入口地址

				AREA    |.text|, CODE, READONLY		;定义代码段，标记为.text

; Reset handler	;利用PROC、ENDP这一对伪指令把程序段分为若干个过程，使程序的结构加清晰
Reset_Handler   PROC								;过程的开始 
				EXPORT  Reset_Handler	[WEAK]		;[WEAK] 弱定义，意思是如果在别处也定义该标号(函数)，在链接时用别处的地址。
				IMPORT  led							;通知编译器要使用的标号在其他文件
				BL		led 						;跳转去执行led函数
				B		.							;原地跳转，即处于循环状态
				ENDP

                ALIGN 								;填充字节使地址对齐
                END									;整个汇编文件结束

