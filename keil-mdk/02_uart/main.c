#include "uart.h"
#include "led.h"

int mymain(void)
{
    uart_init();
    led_init();
    putstring("stm32f103zet6\r\n");

    while(1)
    {
		putstring("led on\r\n");
        led_on();
        delay(1000000);
		
		putstring("led off\r\n");
        led_off();
        delay(1000000);
    }
}

