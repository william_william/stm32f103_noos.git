#include "uart.h"
#include "core_cm3.h"

void exception_init(void)
{
	SCB->SHCSR |= (SCB_SHCSR_USGFAULTENA_Msk);
}
void NMI_Handler(void)
{
	putstring("Exception: NMI.\r\n");
}
void HardFault_Handler(void)
{
	putstring("Exception: Hard Fault.\r\n");
}
void MemManage_Handler(void)
{
	putstring("Exception: Mem Manage Fault.\r\n");
}
void BusFault_Handler(void)
{
	putstring("Exception: Bus Fault.\r\n");
}
void UsageFault_Handler(void)
{
	putstring("Exception: Usage Fault.\r\n");
}
void SVC_Handler(void)
{
	putstring("Exception: SVCall.\r\n");
}
void DebugMon_Handler(void)
{
	putstring("Exception: Debug Monitor.\r\n");
}
void PendSV_Handler(void)
{
	putstring("Exception: PendSV.\r\n");
}
void SysTick_Handler(void)
{
	putstring("Exception: SysTick.\r\n");
}

