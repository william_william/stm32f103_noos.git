
  .syntax unified                   /* 指明当前汇编文件的指令是ARM和THUMB通用格式 */
  .cpu cortex-m3                    /* 指明cpu核为cortex-m3 */
  .fpu softvfp                      /* 软浮点 */
  .thumb                            /* thumb指令 */

.global  _start                     /* .global表示Reset_Handler是一个全局符号 */

.word 0x00000000                    /* 当前地址写入一个字(32bit)数据，值为0x00000000，实际上应为栈顶地址 */
.word _start+1                      /* 当前地址写入一个字(32bit)数据, 值为_reset标号代表的地址+1，即程序入口地址*/

_start:                             /* 标签_start，汇编程序的默认入口是_start */
    /* 1、设置栈 */
    LDR SP, =(0x20000000+0x400)
    /* 2、跳转到led函数 */
    BL led
    /* 3、原地循环 */
    B .
