int delay(int ndelay)
{
	volatile int n = ndelay;
	while(n--);
	
	return 0;
}

int led(void)
{
	unsigned int *pReg;
	
	/* 1、使能GPIOB */
	pReg = (unsigned int *)(0x40021000 + 0x18);
	*pReg |= (1<<3);
	
	/* 2、设置GPIOB5为输出引脚 */
	pReg = (unsigned int *)(0x40010C00 + 0x00);
	*pReg |= (1<<20);

	pReg = (unsigned int *)(0x40010C00 + 0x0C);
	
	while (1)
	{
		/* 3、设置GPIOB5输出1 */
		*pReg |= (1<<5); 
		
		delay(1000000);

		/* 4、设置GPIOB5输出0 */
		*pReg &= ~(1<<5);
		
		delay(1000000);		
	}
}