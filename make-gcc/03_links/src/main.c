#include "uart.h"
#include "led.h"

int mydata = 0x12315;
const int myconst = 0x22315;
int myzero = 0;
int my;

int main(void)
{
    uart_init();
    led_init();
    putstring("stm32f103zet6\r\n");
	putstring("mydata\t:");
    puthex((unsigned int)&mydata);
    puthex((unsigned int)mydata);
    putstring("\r\nmyconst\t:");
    puthex((unsigned int)&myconst);
    puthex((unsigned int)myconst);
	putstring("\r\nmyzero\t:");
    puthex((unsigned int)&myzero);
    puthex((unsigned int)myzero);
    putstring("\r\nmy\t:");
    puthex((unsigned int)&my);
    puthex((unsigned int)my);
    putstring("\r\n");

    while(1)
    {
		putstring("led on\r\n");
        led_on();
        delay(1000000);

		putstring("led off\r\n");
        led_off();
        delay(1000000);
    }
}
