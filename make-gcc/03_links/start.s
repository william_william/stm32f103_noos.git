
  .syntax unified                   /* 指明当前汇编文件的指令是ARM和THUMB通用格式 */
  .cpu cortex-m3                    /* 指明cpu核为cortex-m3 */
  .fpu softvfp                      /* 软浮点 */
  .thumb                            /* thumb指令 */

.global  _start                     /* .global表示Reset_Handler是一个全局符号 */

.word 0x00000000                    /* 当前地址写入一个字(32bit)数据，值为0x00000000，实际上应为栈顶地址 */
.word _start+1                      /* 当前地址写入一个字(32bit)数据, 值为_reset标号代表的地址+1，即程序入口地址*/

_start:                             /* 标签_start，汇编程序的默认入口是_start */
    /* 1、设置栈 */
    LDR SP, =(0x20000000+0x400)

    /* 2、复制data段 */
	movs r1, #0						/* r1寄存器用来计数 */
    b LoopCopyDataInit

CopyDataInit:
    ldr r3, =_sidata				/* 将data段的起始加载地址存入r3寄存器 */
    ldr r3, [r3, r1]				/* 从Flash取出data段的值存入r3寄存器 */
    str r3, [r0, r1]				/* 将r3寄存器的值存入RAM中 */
    adds r1, r1, #4					/* 每次复制4个字节的data段数据 */

LoopCopyDataInit:
    ldr r0, =_sdata					/* 将data段的起始链接地址存入r0寄存器 */
    ldr r3, =_edata					/* 将data段的结束链接地址存入r0寄存器 */
    adds r2, r0, r1					/* 起始链接地址加上计数，即当前复制地址存入r2寄存器 */
    cmp r2, r3						/* 当前复制地址和结束链接地址相比较 */
    bcc CopyDataInit				/* 如果r2小于等于r3跳转到CopyDataInit标签处，如果大于则往下执行 */

    /* 3、清除bss段 */
	ldr r2, =_sbss					/* 将bss段的起始链接地址存入r2寄存器 */
	b LoopFillZerobss

FillZerobss:
	movs r3, #0						/* 将0存入r3寄存器 */
	str r3, [r2], #4				/* 将r3中的值存到r2中的值所指向的地址中, 同时r2中的值加4 */

LoopFillZerobss:
	ldr r3, = _ebss					/* 将bss段的结束链接地址存入r3寄存器 */
	cmp r2, r3						/* 比较r2和r3内的值,即当前清除地址和结束链接地址相比较 */
  	bcc FillZerobss					/* 如果r2小于等于r3跳转到FillZerobss标签处，如果大于则往下执行 */

    /* 4、跳转到led函数 */
    BL main
    /* 5、原地循环 */
    B .
