# stm32f103_noos

#### 介绍
存放STM32F103的裸机编程代码，基于正点原子战舰开发板，同时使用Keil-MDK和arm-none-eabi-gcc工具链


简易的命令行入门教程:
Git 全局设置:
```bash
git config --global user.name "william"
git config --global user.email "472853046@qq.com"
```

创建 git 仓库:
```bash
mkdir stm32f103_noos
cd stm32f103_noos
git init
touch README.md
git add README.md
git commit -m "first commit"
git remote add origin https://gitee.com/william_william/stm32f103_noos.git
git push -u origin master
```
同步仓库:
```bash
git pull origin master
